import React from "react";

import { Tab, Tabs, AppBar, Button, Box, Paper } from "@material-ui/core";

import "./App.css";
import "../MonthTable/MonthModal.css";
import "../AmmoTable/AmmoTable.css";
import AppHeader from "../AppHeader/AppHeader";

import MonthTable from "../MonthTable/MonthTable";
import MonthModal from "../MonthTable/MonthModal";

import AmmoTable from "../AmmoTable/AmmoTable";
import AmmoSearchBar from "../AmmoTable/AmmoSearchBar";
import AmmoSearchModal from "../AmmoTable/AmmoSearchModal/AmmoSearchModal";

import FuelTable from "../FuelTable/FuelTable";
import MonthShipName from "../MonthTable/MonthShipName";

import SideNavBar from "../SideNavBar/SideNavBar";
import ResourceAllocationModal from "../AmmoTable/ResourceAllocationModal";

const TabPanel = ({ children, value, index }) => {
  if (value === index) {
    return <Box>{children}</Box>;
  } else return null;
};

function createDataMonths(name, year, months) {
  return { name, year, months };
}

function createDataAmmo(
  shipAmmoName,
  allocationType,
  startDate,
  endDate,
  category,
  nameOfAmmo,
  amount,
  note
) {
  return {
    shipAmmoName,
    allocationType,
    startDate,
    endDate,
    category,
    nameOfAmmo,
    amount,
    note,
  };
}

export default class App extends React.Component {
  constructor() {
    super();
    this.handleTabs = this.handleTabs.bind(this);
    this.handleChangeIndex = this.handleChangeIndex.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.typeAlert = this.typeAlert.bind(this);
    this.closeTypeAlert = this.closeTypeAlert.bind(this);
    this.OpenResAllocModal = this.OpenResAllocModal.bind(this);
    this.CloseResAllocModal = this.CloseResAllocModal.bind(this);

    this.state = {
      value: 0,
      open: false,
      showSearchModal: false,
      resAllocModal: false,
      shipName: "",
      monthsName: "",
      yearName: "",
      rowsOfMonths: [
        createDataMonths("Ship 1", 1998, ["ספטמבר", "אוקטובר", "נובמבר"]),
        createDataMonths("Ship 2", 2001, ["דצמבר", "נובמבר", "מרץ"]),
        createDataMonths("Ship 3", 2004, ["פברואר", "מאי", "אוגוסט"]),
      ],

      shipAmmoName: "",
      allocationType: "",
      startDate: "",
      endDate: "",
      category: "",
      nameOfAmmo: "",
      amount: "",
      note: "",

      rowsOfAmmo: [
        createDataAmmo(
          "Ship 2",
          "4",
          "31.12.2020",
          "1.1.2020",
          "Annual",
          "The drop dropped",
          4000,
          " - "
        ),
      ],
    };
  }

  handleTabs = (event, value) => {
    console.log(value);
    this.setState({
      value: value,
    });
  };

  handleChangeIndex = (index) => {
    this.setState({
      value: index,
    });
  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  typeAlert = () => {
    this.setState({ showSearchModal: true });
  };

  closeTypeAlert = () => {
    this.setState({ showSearchModal: false });
  };

  OpenResAllocModal = () => {
    this.setState({ resAllocModal: true });
  };

  CloseResAllocModal = () => {
    this.setState({ resAllocModal: false });
  };

  SaveActivityShipName = () => {
    this.state.rowsOfMonths.push(
      createDataMonths(
        this.state.shipName,
        this.state.yearName,
        this.state.monthsName
      )
    );
    this.setState({
      rowsOfMonths: this.state.rowsOfMonths,
    });
    this.handleClose();
    console.log(this.state.monthsName);
  };

  ChangeShipName = (e) => {
    this.setState({ shipName: e.target.value });
  };

  changeActivityYearState = (year) => {
    this.setState({
      yearName: year,
    });
    console.log(year);
  };

  changeActivityMonthsState = (months) => {
    this.setState({
      monthsName: months,
    });
    console.log(months);
  };

  SaveAmmoShipData = () => {
    this.state.rowsOfAmmo.push(
      createDataAmmo(
        this.state.shipAmmoName,
        this.state.allocationType,
        this.state.startDate,
        this.state.endDate,
        this.state.category,
        this.state.nameOfAmmo,
        this.state.amount,
        this.state.note
      )
    );
    this.setState({
      rowsOfAmmo: this.state.rowsOfAmmo,
    });

    this.CloseResAllocModal();
    console.log(this.state.rowsOfAmmo);
  };

  changeShipAmmoName = (e) => {
    this.setState({ shipAmmoName: e.target.value });
  };

  changeAmmoAllocationType = (alloc) => {
    this.setState({
      allocationType: alloc,
    });
    console.log(alloc);
  };

  ChangeAmmoStartDate = (e) => {
    this.setState({
      startDate: e.target.value,
    });
  };

  ChangeAmmoEndDate = (e) => {
    this.setState({
      endDate: e.target.value,
    });
  };

  ChangeAmmoCategory = (categ) => {
    this.setState({
      category: categ,
    });
  };

  AddAmmoNote = (e) => {
    this.setState({
      note: e.target.value,
    });
  };

  render() {
    return (
      <div className="App">
        <AmmoSearchModal
          open={this.state.showSearchModal}
          closeTypeAlert={this.closeTypeAlert}
        />

        <ResourceAllocationModal
          open={this.state.resAllocModal}
          handleClose={this.handleClose}
          SaveAmmoShipData={this.SaveAmmoShipData}
          CloseResAllocModal={this.CloseResAllocModal}
          shipAmmoName={this.state.shipAmmoName}
          changeShipAmmoName={this.changeShipAmmoName}
          startDate={this.state.startDate}
          ChangeAmmoStartDate={this.ChangeAmmoStartDate}
          endDate={this.state.endDate}
          ChangeAmmoEndDate={this.ChangeAmmoEndDate}
          allocationType={this.state.allocationType}
          changeAmmoAllocationType={this.changeAmmoAllocationType}
          category={this.state.category}
          ChangeAmmoCategory={this.ChangeAmmoCategory}
          note={this.state.note}
          AddAmmoNote={this.AddAmmoNote}
        />
        <AppHeader />
        <div align="right">
          <SideNavBar align="right" handleClose={this.handleClose} />

          <AppBar position="static">
            <Tabs
              value={this.state.value}
              onChange={this.handleTabs}
              centered
              className={"TabsCenter"}
            >
              <Tab label="דלק" />
              <Tab label="תחמושת" />
              <Tab label="חודשי פעילות" />
            </Tabs>
          </AppBar>

          <Paper
            index={this.state.value}
            onChangeIndex={this.handleChangeIndex}
          >
            <TabPanel value={this.state.value} index={0}>
              <Box>
                <FuelTable />
              </Box>
            </TabPanel>
            <TabPanel value={this.state.value} index={1}>
              <Box className={"TopBox"}>
                <AmmoSearchBar align="right" typeAlert={this.typeAlert} />
                <Button className={"BtnColor"} onClick={this.OpenResAllocModal}>
                  הוסף
                </Button>
                <AmmoTable rowsOfAmmo={this.state.rowsOfAmmo} />
              </Box>
            </TabPanel>
            <TabPanel value={this.state.value} index={2}>
              <Box className={"TopBox"}>
                <MonthShipName
                  shipName={this.state.shipName}
                  ChangeShipName={this.ChangeShipName}
                />
                <Button className={"BtnColor"} onClick={this.handleOpen}>
                  הוסף
                </Button>
                <MonthModal
                  open={this.state.open}
                  handleClose={this.handleClose}
                  SaveActivityShipName={this.SaveActivityShipName}
                  changeActivityMonthsState={this.changeActivityMonthsState}
                  changeActivityYearState={this.changeActivityYearState}
                />
                <MonthTable rowsOfMonths={this.state.rowsOfMonths} />
              </Box>
            </TabPanel>
          </Paper>
        </div>
      </div>
    );
  }
}
