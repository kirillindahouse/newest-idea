import React from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@material-ui/core";

import "./MonthModal.css";

const MonthTable = ({ rowsOfMonths }) => {
  return (
    <TableContainer>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell align="right">חודשי פעילות</TableCell>
            <TableCell align="right">שנה</TableCell>
            <TableCell align="right">שם ספינה</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rowsOfMonths.map((row) => (
            <TableRow key={row.name}>
              <TableCell align="right">{row.months.toString()}</TableCell>
              <TableCell align="right">{row.year}</TableCell>
              <TableCell align="right" component="th" scope="row">
                {row.name}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default MonthTable;
