import React from "react";
import { TextField, InputAdornment } from "@material-ui/core";
import "./MonthModal.css";

const MonthShipName = ({ shipName, ChangeShipName }) => {
  console.log(shipName);
  return (
    <div align="center">
      <TextField
        InputProps={{
          endAdornment: <InputAdornment position="end"> :ספינה</InputAdornment>,
        }}
        align="center"
        value={shipName}
        onChange={ChangeShipName}
      ></TextField>
    </div>
  );
};
export default MonthShipName;
