import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Modal, Paper, Button, ButtonGroup, Box } from "@material-ui/core";
import YearDropDownList from "./YearDropDownList";
import MonthCheckboxes from "./MonthCheckboxes";

const modalStyle = makeStyles(() => ({
  root: {
    position: "absolute",
    width: 400,
    top: `${50}%`,
    left: `${50}%`,
    transform: `translate(-${50}%, -${50}%)`,
  },
}));

const MonthModal = ({
  open,
  handleClose,
  SaveActivityShipName,
  changeActivityMonthsState,
  changeActivityYearState,
}) => {
  const classes = modalStyle();

  const Headline = () => {
    return <Box>חודשי פעילות</Box>;
  };

  return (
    <Modal open={open} onClose={handleClose}>
      <Paper className={classes.root} align="center">
        <Headline />
        <YearDropDownList
          className="DropDownList"
          changeActivityYearState={changeActivityYearState}
        />
        <MonthCheckboxes
          changeActivityMonthsState={changeActivityMonthsState}
        />
        <ButtonGroup className="BackColor">
          <Button onClick={SaveActivityShipName} className="BtnsColor">
            שמור
          </Button>
        </ButtonGroup>
      </Paper>
    </Modal>
  );
};
export default MonthModal;
