import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import {
  Checkbox,
  FormGroup,
  FormControlLabel,
  FormControl,
  Button,
  ButtonGroup,
} from "@material-ui/core";

export default function MonthCheckboxes({ changeActivityMonthsState }) {
  const [state, setState] = React.useState({
    0: false,
    1: false,
    2: false,
    3: true,
    4: false,
    5: false,
    6: true,
    7: false,
    8: false,
    9: true,
    10: false,
    11: false,
  });

  const CancelAllBtn = () => {
    return (
      <div>
        <Button
          className="BtnsColor"
          onClick={() => {
            const months = { ...state }; //копирую объект
            for (let prop in months) {
              months[prop] = false;
            }
            setState(months);
            const monthString = [];
            for (let m in months) {
              // m - object's key
              if (months[m] === true) {
                monthString.push(monthsArr[m]);
              }
            }
            changeActivityMonthsState(monthString);
          }}
        >
          בטל בחירות
        </Button>
      </div>
    );
  };

  const SelectAllBtn = () => {
    return (
      <div>
        <Button
          className="BtnsColor"
          onClick={() => {
            const months = { ...state }; //копирую объект
            for (let prop in months) {
              months[prop] = true;
            }
            setState(months);
            const monthString = [];
            for (let m in months) {
              // m - object's key
              if (months[m] === true) {
                monthString.push(monthsArr[m]);
              }
            }
            changeActivityMonthsState(monthString);
          }}
        >
          בחר הכל
        </Button>
      </div>
    );
  };

  const monthsArr = [
    "ינואר",
    "פברואר",
    "מרץ",
    "אפריל",
    "מאי",
    "יוני",
    "יולי",
    "אוגוסט",
    "ספטמבר",
    "אוקטובר",
    "נובמבר",
    "דצמבר",
  ];

  const handleChange = (i, event) => {
    const months = { ...state, [event.target.id]: event.target.checked };
    setState(months);

    const monthString = [];
    for (let m in months) {
      // m - object's key
      if (months[m] === true) {
        monthString.push(monthsArr[m]);
      }
    }
    changeActivityMonthsState(monthString);
  };

  const monthsArrMadeFromForEach = [];

  monthsArr.forEach((item, i) =>
    monthsArrMadeFromForEach.push(
      <FormControlLabel
        key={i}
        control={
          <Checkbox
            color="primary"
            onChange={(event) => handleChange(monthsArr[i], event)}
            id={i}
            checked={state[i]}
          />
        }
        label={item}
        labelPlacement="start"
      />
    )
  );

  return (
    <div>
      <ButtonGroup className="BackColorModal">
        <SelectAllBtn />
        <CancelAllBtn />
      </ButtonGroup>

      <FormControl>
        <FormGroup>{monthsArrMadeFromForEach.slice(6, 12)}</FormGroup>
      </FormControl>
      <FormControl>
        <FormGroup>{monthsArrMadeFromForEach.slice(0, 6)}</FormGroup>
      </FormControl>
    </div>
  );
}
