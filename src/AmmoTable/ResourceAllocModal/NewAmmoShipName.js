import React from "react";
import { TextField, InputAdornment } from "@material-ui/core";

const NewAmmoShipName = ({ shipAmmoName, changeShipAmmoName }) => {
  console.log(shipAmmoName);
  return (
    <div>
      <TextField
        InputProps={{
          endAdornment: <InputAdornment position="end"> :ספינה</InputAdornment>,
        }}
        align="center"
        value={shipAmmoName}
        onChange={changeShipAmmoName}
      ></TextField>
    </div>
  );
};

export default NewAmmoShipName;
