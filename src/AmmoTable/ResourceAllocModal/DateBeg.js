import React from "react";
import { TextField, InputAdornment } from "@material-ui/core";

const DateBeg = ({ startDate, ChangeAmmoStartDate }) => {
  console.log(startDate);
  return (
    <div>
      <TextField
        InputProps={{
          endAdornment: (
            <InputAdornment position="start"> :תתאריך התחלה</InputAdornment>
          ),
        }}
        align="center"
        value={startDate}
        onChange={ChangeAmmoStartDate}
      ></TextField>
    </div>
  );
};

export default DateBeg;
