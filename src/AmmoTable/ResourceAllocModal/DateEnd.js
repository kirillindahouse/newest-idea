import React from "react";
import { TextField, InputAdornment } from "@material-ui/core";

const DateBeg = ({ endDate, ChangeAmmoEndDate }) => {
  console.log(endDate);
  return (
    <div>
      <TextField
        InputProps={{
          endAdornment: (
            <InputAdornment position="start"> :תאריך סיום</InputAdornment>
          ),
        }}
        align="center"
        value={endDate}
        onChange={ChangeAmmoEndDate}
      ></TextField>
    </div>
  );
};

export default DateBeg;
