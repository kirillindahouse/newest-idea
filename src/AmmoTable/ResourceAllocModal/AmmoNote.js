import React from "react";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";

const AmmoNote = ({ note, AddAmmoNote }) => {
  console.log(note);
  return (
    <TextareaAutosize
      aria-label="minimum height"
      rowsMin={9}
      value={note}
      onChange={AddAmmoNote}
    />
  );
};

export default AmmoNote;
