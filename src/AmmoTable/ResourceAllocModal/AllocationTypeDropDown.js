import React from "react";

import { MenuItem, Box, FormControl, Select } from "@material-ui/core";

export default function AllocationTypeDropDow({ changeAmmoAllocationType }) {
  const [alloc, setYear] = React.useState("");
  const [open, setOpen] = React.useState(false);

  const handleChange = (event) => {
    setYear(event.target.value);
    const allocationType = event.target.value;
    changeAmmoAllocationType(allocationType);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  return (
    <div>
      <FormControl align="right">
        <Box>:סוג הקצאה</Box>
        <Select
          open={open}
          onClose={handleClose}
          onOpen={handleOpen}
          value={alloc}
          onChange={handleChange}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={0}>0</MenuItem>
          <MenuItem value={1}>1</MenuItem>
          <MenuItem value={2}>2</MenuItem>
          <MenuItem value={3}>3</MenuItem>
        </Select>
      </FormControl>
      <Box> </Box>
    </div>
  );
}
