import React from "react";

import { MenuItem, Box, FormControl, Select } from "@material-ui/core";

export default function Categories({ ChangeAmmoCategory }) {
  const [categ, setYear] = React.useState("");
  const [open, setOpen] = React.useState(false);

  const handleChange = (event) => {
    setYear(event.target.value);
    const categoryType = event.target.value;
    ChangeAmmoCategory(categoryType);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  return (
    <div>
      <FormControl align="right">
        <Box>:קטגוריה</Box>
        <Select
          open={open}
          onClose={handleClose}
          onOpen={handleOpen}
          value={categ}
          onChange={handleChange}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value="Annual">Annual</MenuItem>
          <MenuItem value="Monthly">Monthly</MenuItem>
          <MenuItem value="Daily">Daily</MenuItem>
        </Select>
      </FormControl>
      <Box> </Box>
    </div>
  );
}
