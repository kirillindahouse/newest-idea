import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import InputBase from "@material-ui/core/InputBase";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "2px 4px",
    display: "flex",
    alignItems: "center",
    width: 200,
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

export default function NameAmmo({ typeAlert, closeTypeAlert }) {
  const classes = useStyles();

  return (
    <Paper
      justify="center"
      className="AmmoSearchPosition"
      className={classes.root}
    >
      <IconButton className={classes.iconButton} aria-label="menu">
        <SearchIcon />
      </IconButton>
      <InputBase className={classes.input} />
      <IconButton
        className={classes.iconButton}
        aria-label="search"
        closeTypeAlert={closeTypeAlert}
        onClick={typeAlert}
      ></IconButton>
    </Paper>
  );
}
