import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Modal,
  Paper,
  Button,
  ButtonGroup,
  Box,
  TextareaAutosize,
  FormControl,
  FormGroup,
} from "@material-ui/core";
import AllocationTypeDropDow from "./ResourceAllocModal/AllocationTypeDropDown";
import Categories from "./ResourceAllocModal/Categories";
import NameAmmo from "./ResourceAllocModal/NameAmmo";
import "./AmmoTable.css";

const modalStyle = makeStyles(() => ({
  root: {
    position: "absolute",
    width: 400,
    top: `${50}%`,
    left: `${50}%`,
    transform: `translate(-${50}%, -${50}%)`,
  },
}));

const AmmoModal = ({ open, SaveAmmoShipName }) => {
  const classes = modalStyle();

  const Headline = () => {
    return (
      <p align="center">
        <b>הקצאת משאב</b>
      </p>
    );
  };

  const OrganizUnit = () => {
    return <Box>:יחידה ארגונית</Box>;
  };

  const DateBeg = () => {
    return <Box>:תאריך התחלה</Box>;
  };

  const DateEnd = () => {
    return <Box>:תאריך סיום</Box>;
  };

  const NameOfNameAmmo = () => {
    return <Box>:שם תחמושת</Box>;
  };

  const Note = () => {
    return <TextareaAutosize aria-label="minimum height" rowsMin={9} />;
  };

  return (
    <Modal open={open}>
      <Paper className={classes.root} align="right">
        <Headline />
        <FormControl>
          <FormGroup>
            <Box>:הערה</Box>
            <Note />
          </FormGroup>
        </FormControl>

        <OrganizUnit />

        <AllocationTypeDropDow />
        <DateBeg />
        <DateEnd />
        <Categories />
        <NameOfNameAmmo />
        <NameAmmo />

        <ButtonGroup className="BackColor">
          <Button onClick={SaveAmmoShipName} className="BtnColor">
            שמור
          </Button>
        </ButtonGroup>
      </Paper>
    </Modal>
  );
};
export default AmmoModal;
