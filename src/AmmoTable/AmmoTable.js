import React from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@material-ui/core";

const AmmoTable = ({ rowsOfAmmo }) => {
  return (
    <TableContainer>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell align="right">הערה</TableCell>
            <TableCell align="right">כמות</TableCell>
            <TableCell align="right">שם תחמושת</TableCell>
            <TableCell align="right">קטגוריה</TableCell>
            <TableCell align="right">תאריך סיום</TableCell>
            <TableCell align="right">תאריך התחלה</TableCell>
            <TableCell align="right">סוג הקצאה</TableCell>
            <TableCell align="right">שם ספינה</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rowsOfAmmo.map((row) => (
            <TableRow key={row.name}>
              <TableCell align="right">{row.note}</TableCell>
              <TableCell align="right">{row.amount}</TableCell>
              <TableCell align="right">{row.nameOfAmmo}</TableCell>
              <TableCell align="right">{row.category}</TableCell>
              <TableCell align="right">{row.endDate}</TableCell>
              <TableCell align="right">{row.startDate}</TableCell>
              <TableCell align="right">{row.allocationType}</TableCell>
              <TableCell align="right">{row.shipAmmoName}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default AmmoTable;
