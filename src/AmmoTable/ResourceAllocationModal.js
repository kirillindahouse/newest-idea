import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Modal,
  Paper,
  Button,
  ButtonGroup,
  Box,
  FormControl,
  FormGroup,
} from "@material-ui/core";
import AllocationTypeDropDow from "../AmmoTable/ResourceAllocModal/AllocationTypeDropDown";
import Categories from "./ResourceAllocModal/Categories";
import NameAmmo from "./ResourceAllocModal/NameAmmo";
import NewAmmoShipName from "./ResourceAllocModal/NewAmmoShipName";
import DateBeg from "./ResourceAllocModal/DateBeg";
import DateEnd from "./ResourceAllocModal/DateEnd";
import AmmoNote from "./ResourceAllocModal/AmmoNote";

const modalStyle = makeStyles(() => ({
  root: {
    position: "absolute",
    width: 400,
    top: `${50}%`,
    left: `${50}%`,
    transform: `translate(-${50}%, -${50}%)`,
  },
}));

const ResourceAllocationModal = ({
  open,
  CloseResAllocModal,
  changeShipAmmoName,
  shipAmmoName,
  SaveAmmoShipData,
  startDate,
  ChangeAmmoStartDate,
  endDate,
  ChangeAmmoEndDate,
  allocationType,
  changeAmmoAllocationType,
  category,
  ChangeAmmoCategory,
  note,
  AddAmmoNote,
}) => {
  const classes = modalStyle();

  const Headline = () => {
    return (
      <p align="center">
        <b>הקצאת משאב</b>
      </p>
    );
  };

  const OrganizUnit = () => {
    return <Box>:יחידה ארגונית</Box>;
  };

  const NameOfNameAmmo = () => {
    return <Box>:שם תחמושת</Box>;
  };

  return (
    <Modal open={open} onClose={CloseResAllocModal}>
      <Paper className={classes.root} align="right">
        <Paper>
          <Headline />

          <FormControl>
            <FormGroup>
              <OrganizUnit />
              <NewAmmoShipName
                shipAmmoName={shipAmmoName}
                changeShipAmmoName={changeShipAmmoName}
              />
              <AllocationTypeDropDow
                allocationType={allocationType}
                changeAmmoAllocationType={changeAmmoAllocationType}
              />
              <DateBeg
                startDate={startDate}
                ChangeAmmoStartDate={ChangeAmmoStartDate}
              />
              <DateEnd
                endDate={endDate}
                ChangeAmmoEndDate={ChangeAmmoEndDate}
              />
              <Categories
                category={category}
                ChangeAmmoCategory={ChangeAmmoCategory}
              />
              <NameOfNameAmmo />
              <NameAmmo />
            </FormGroup>
          </FormControl>

          <FormGroup>
            <Box>:הערה</Box>
            <AmmoNote note={note} AddAmmoNote={AddAmmoNote} />
          </FormGroup>
        </Paper>

        <Paper align="center">
          <ButtonGroup className="BackColor">
            <Button onClick={SaveAmmoShipData} className="BtnsColor">
              שמור
            </Button>
          </ButtonGroup>
        </Paper>
      </Paper>
    </Modal>
  );
};
export default ResourceAllocationModal;
