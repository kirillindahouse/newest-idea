import React from "react";

import { MenuItem, Box, FormControl, Select } from "@material-ui/core";

export default function AmmoSearchCategoryDrop() {
  const [year, setYear] = React.useState("");
  const [open, setOpen] = React.useState(false);

  const handleChange = (event) => {
    setYear(event.target.value);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  return (
    <div>
      <FormControl align="right">
        <Box>קטגוריה: </Box>
        <Select
          open={open}
          onClose={handleClose}
          onOpen={handleOpen}
          value={year}
          onChange={handleChange}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={1}>1</MenuItem>
          <MenuItem value={2}>2</MenuItem>
          <MenuItem value={3}>3</MenuItem>
          <MenuItem value={4}>4</MenuItem>
        </Select>
      </FormControl>
    </div>
  );
}
