import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Modal, Paper, Button, ButtonGroup, Box } from "@material-ui/core";
import AmmoSearchYearDrop from "../AmmoSearchModal/AmmoShearchYearDrop";
import AmmoSearchCategoryDrop from "../AmmoSearchModal/AmmoSearchCategoryDrop";

const modalStyle = makeStyles(() => ({
  root: {
    position: "absolute",
    width: 400,
    top: `${50}%`,
    left: `${50}%`,
    transform: `translate(-${50}%, -${50}%)`,
  },
}));

const AmmoSearchModal = ({ open, closeTypeAlert }) => {
  const classes = modalStyle();

  const Headline = () => {
    return <Box>חודשי פעילות</Box>;
  };

  return (
    <Modal open={open}>
      <Paper className={classes.root} align="center">
        <AmmoSearchYearDrop />
        <AmmoSearchCategoryDrop />
        <ButtonGroup className="BackColor">
          <Button onClick={closeTypeAlert} className="BtnsColor">
            שמור
          </Button>
        </ButtonGroup>
      </Paper>
    </Modal>
  );
};

export default AmmoSearchModal;
