import React from "react";

import { MenuItem, Box, FormControl, Select } from "@material-ui/core";

export default function AmmoSearchYearModal() {
  const [year, setYear] = React.useState("");
  const [open, setOpen] = React.useState(false);

  const handleChange = (event) => {
    setYear(event.target.value);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  return (
    <div>
      <FormControl align="right">
        <Box>שנה: </Box>
        <Select
          open={open}
          onClose={handleClose}
          onOpen={handleOpen}
          value={year}
          onChange={handleChange}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={1995}>1995</MenuItem>
          <MenuItem value={1996}>1996</MenuItem>
          <MenuItem value={1997}>1997</MenuItem>
          <MenuItem value={1998}>1998</MenuItem>
        </Select>
      </FormControl>
    </div>
  );
}
