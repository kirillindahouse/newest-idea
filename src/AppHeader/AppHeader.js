import React from "react";

const AppHeader = () => {
  return (
    <div>
      <h1 className="App-lit-color"> כשירות מבצעים </h1>
      <h2 className="App-lit-color">הקצאת משבים</h2>
    </div>
  );
};
export default AppHeader;
