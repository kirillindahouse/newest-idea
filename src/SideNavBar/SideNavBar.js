import React from "react";
import {
  Divider,
  Box,
  List,
  ListItem,
  ListItemText,
  Drawer,
  IconButton,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";

const SideNavBar = () => {
  const [state, setState] = React.useState({
    left: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const SideNames = [
    "יצירת דוח חזרה מהים",
    "חיפוש דוח חזרה מהים",
    "הבקשות שלי",
    "הקצאת משאבים",
  ];

  const list = (anchor) => (
    <div>
      <List>
        {SideNames.map((text, index) => (
          <Box>
            <ListItem
              button
              key={index}
              onClick={() => {
                if (text === SideNames[3]) {
                  {
                    alert("click");
                  }
                }
              }}
            >
              <ListItemText primary={text} />
            </ListItem>
            <Divider />
          </Box>
        ))}
      </List>
    </div>
  );

  return (
    <div>
      {["right"].map((anchor) => (
        <React.Fragment key={anchor}>
          <IconButton align="start" onClick={toggleDrawer(anchor, true)}>
            <MenuIcon />
          </IconButton>

          <Drawer
            anchor={anchor}
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
          >
            {list(anchor)}
          </Drawer>
        </React.Fragment>
      ))}
    </div>
  );
};
export default SideNavBar;
